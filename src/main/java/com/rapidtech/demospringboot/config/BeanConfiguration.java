package com.rapidtech.demospringboot.config;

import com.rapidtech.demospringboot.model.Foo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class BeanConfiguration {
@Bean
    public Foo foo(){
    Foo foo = new Foo();
    log.info("Created bean foo..");
    return foo;
}

}
