package com.rapidtech.demospringboot.singleton;

public class Bank {
    public static String name;

    public static String getName(){
        if(name == null){
            name = new String();
        }
        return name;
    }

    private Bank() {

    }
}
