package com.rapidtech.demospringboot.cylic;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CyclicB {
private CyclicC cyclicC;
}
