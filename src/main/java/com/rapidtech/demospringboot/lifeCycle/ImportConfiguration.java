package com.rapidtech.demospringboot.lifeCycle;


import com.rapidtech.demospringboot.config.BeanConfiguration;
import com.rapidtech.demospringboot.config.DuplicateBeanConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value ={
        BeanConfiguration.class,
        DuplicateBeanConfiguration.class
})
public class ImportConfiguration {

}
