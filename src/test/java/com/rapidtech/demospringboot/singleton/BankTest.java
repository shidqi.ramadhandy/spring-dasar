package com.rapidtech.demospringboot.singleton;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BankTest {

    @Test
    void singleTonTest(){
        String name1 = Bank.getName();
        String name2 = Bank.getName();

        Assertions.assertSame(name1, name2);
    }

}